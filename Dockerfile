FROM python:alpine

COPY src /yadtt
WORKDIR /yadtt

RUN pip install poetry
RUN poetry config settings.virtualenvs.create false && poetry install --no-interaction

EXPOSE 8080

CMD ["gunicorn", "--bind", "0.0.0.0:8080", "yadtt.wsgi", "--log-file=-"]
