import django_filters
from django_filters.rest_framework import FilterSet

from core.models import Character, get_faction_races


class CharacterFilter(FilterSet):
    id = django_filters.AllValuesMultipleFilter(field_name="pk")
    guild = django_filters.NumberFilter(field_name="guild__id")
    noguild = django_filters.BooleanFilter(field_name="guild", lookup_expr="isnull")
    name = django_filters.CharFilter(field_name="name", lookup_expr="icontains")
    faction = django_filters.CharFilter(method="faction_filter")

    class Meta:
        model = Character
        fields = ("user",)

    def faction_filter(self, queryset, name, value):
        if not value:
            return queryset

        return queryset.filter(race__in=get_faction_races(value))


class ActivityFilter(FilterSet):
    guild = django_filters.NumberFilter(field_name="guild__id")


class BossFilter(FilterSet):
    activity = django_filters.NumberFilter(field_name="activity__id")
    guild = django_filters.NumberFilter(field_name="activity__guild__id")
