from guardian.shortcuts import get_objects_for_user
from rest_framework_jwt.utils import jwt_payload_handler as default_jwt_payload_handler

from core.models import Guild


def jwt_payload_handler(user):
    payload = default_jwt_payload_handler(user)
    payload["manageableGuilds"] = [
        guild.id for guild in get_objects_for_user(user, "core.change_guild", Guild)
    ]

    return payload
