"""Permissions

Guilds:
    - list: anybody (anybody should be able to retrieve a list of guilds and their names)
    - create: anybody
    - details: core.view_guild_details
    - update: core.change_guild
    - delete: core.delete_guild

    -> DjangoObjectPermissions

    - remove characters: core.change_guild *or* core.change_character on the character
    - award dkp: core.change_guild

Characters:
    - list: anybody (anybody should be able to retrieve a list of characters and their basic attributes)
    - create: anybody
    - details: anybody
    - update: core.change_character
    - delete: core.delete_character

Guild Invites:
    - list: core.change_guild on parent guild *or* if invitee is a character of the logged-in user
    - create: core.change_guild on parent guild *and only for that guild* and not for characters of the opposing faction
    - update: *nobody*
    - delete: core.change_guild on parent guild

    - accept: core.change_character for the invitee

Events:
    - list: core.view_character on parent character
    - create: core.change_guild on parent guild
    - update: *nobody*
    - delete: *nobody*

Activity:
    - any: core.change_guild on parent guild
    - TBD: probably gonna remove this model

Boss:
    - any: core.change_guild on the parent activity's guild
    - TBD: probably gonna remove this model
"""
from rest_framework import permissions
from rest_framework.exceptions import NotFound

from core import models
from core.models import Guild, Character


class GuildInvitePermission(permissions.BasePermission):
    def has_permission(self, request, view):
        """Only people who can manage the guild are allowed to create and delete invites."""
        if request.method in permissions.SAFE_METHODS:
            return True

        guild_id = request.data.get("guild")
        try:
            guild = Guild.objects.get(pk=guild_id)
            if request.user.has_perm("core.change_guild", guild):
                return True
        except Guild.DoesNotExist:
            raise NotFound("Guild with id {} was not found".format(guild_id))

        return False


class AcceptGuildInvitePermission(permissions.BasePermission):
    def has_object_permission(self, request, view, invite: models.GuildInvite):
        # This is always a POST request
        # Only if the logged in user is the invitee can the invite be accepted
        if view.action == "accept":
            return invite.invitee.user == request.user
        elif request.user.has_perm("core.change_guild", invite.guild):
            return True

        return False


class RemoveCharacterFromGuildPermission(permissions.BasePermission):
    def has_object_permission(self, request, view, guild: Guild):
        """Guild managers can remove any character and members can remove their own characters from guilds."""
        # User can manager guild.
        if request.user.has_perm("core.change_guild", guild):
            return True

        character_id = request.data.get("character")

        try:
            request.user.characters.get(pk=character_id)
            # User is trying to remove their own character from it's current guild.
            return True
        except Character.DoesNotExist:
            return False


class EventPermission(permissions.BasePermission):
    def has_object_permission(self, request, view, event: models.Event):
        if request.method in permissions.SAFE_METHODS:
            return True

        if request.method == "POST" and request.user.has_perm(
            "core.change_guild", event.guild
        ):
            return True

        return False
