from rest_framework.fields import CharField, IntegerField
from rest_framework.serializers import ModelSerializer

from core import models


class GuildSerializer(ModelSerializer):
    member_count = IntegerField(read_only=True)

    class Meta:
        model = models.Guild
        fields = "__all__"


class CharacterSerializer(ModelSerializer):
    guild = GuildSerializer(read_only=True)
    total_dkp = IntegerField(read_only=True)

    class Meta:
        model = models.Character
        exclude = ("char_class",)


# This is damned ugly but the only way to use 'class' as a field name.
# noinspection PyProtectedMember
CharacterSerializer._declared_fields["class"] = CharField(source="char_class")


class GuildInviteSerializer(ModelSerializer):
    class Meta:
        model = models.GuildInvite
        fields = "__all__"
        read_only_fields = ("inviter", "created")


class EventSerializer(ModelSerializer):
    class Meta:
        model = models.Event
        fields = "__all__"
