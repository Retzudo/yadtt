from django.contrib.auth.models import User
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from core.models import Character


class CharacterTestCase(APITestCase):
    def setUp(self):
        self.user_one_creds = {"username": "userone", "password": "userone"}
        self.user_two_creds = {"username": "usertwo", "password": "usertwo"}
        self.user_one = User.objects.create_user(**self.user_one_creds)
        self.user_two = User.objects.create_user(**self.user_two_creds)

    def test_list_characters(self):
        Character.objects.create(
            name="Testchar",
            race="gnome",
            gender="female",
            role="dd",
            char_class="rogue",
            user=self.user_one,
        )

        self.client.login(**self.user_one_creds)

        response = self.client.get(path=reverse("characters-list")).json()

        self.assertEqual(len(response), 1)

    def test_create_character(self):
        self.client.login(**self.user_one_creds)

        response = self.client.post(
            path=reverse("characters-list"),
            data={
                "name": "Testchar",
                "race": "gnome",
                "gender": "female",
                "role": "dd",
                "class": "rogue",
            },
        )

        character = response.json()

        self.assertEqual(response.status_code, 201)
        self.assertEqual(character.get("name"), "Testchar")
        self.assertEqual(character.get("race"), "gnome")

    def test_update_character(self):
        self.client.login(**self.user_one_creds)

        character_id = (
            self.client.post(
                path=reverse("characters-list"),
                data={
                    "name": "Testchar",
                    "race": "gnome",
                    "gender": "female",
                    "role": "dd",
                    "class": "rogue",
                },
            )
            .json()
            .get("id")
        )

        updated = {
            "name": "Testchar",
            "race": "human",
            "gender": "female",
            "role": "dd",
            "class": "rogue",
        }

        response = self.client.put(
            path=reverse("characters-detail", kwargs={"pk": character_id}), data=updated
        )

        character = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(character.get("name"), "Testchar")
        self.assertEqual(character.get("race"), "human")

        self.client.login(**self.user_two_creds)

        response = self.client.put(
            path=reverse("characters-detail", kwargs={"pk": character_id}), data=updated
        )

        self.assertEqual(response.status_code, 403)

    def test_delete_character(self):
        self.client.login(**self.user_one_creds)

        character_id = (
            self.client.post(
                path=reverse("characters-list"),
                data={
                    "name": "Testchar",
                    "race": "gnome",
                    "gender": "female",
                    "role": "dd",
                    "class": "rogue",
                },
            )
            .json()
            .get("id")
        )

        response = self.client.delete(
            path=reverse("characters-detail", kwargs={"pk": character_id})
        )

        self.assertEqual(response.status_code, 204)

        character_id = (
            self.client.post(
                path=reverse("characters-list"),
                data={
                    "name": "Testchar",
                    "race": "gnome",
                    "gender": "female",
                    "role": "dd",
                    "class": "rogue",
                },
            )
            .json()
            .get("id")
        )

        self.client.login(**self.user_two_creds)

        response = self.client.delete(
            path=reverse("characters-detail", kwargs={"pk": character_id})
        )

        self.assertEqual(response.status_code, 403)
