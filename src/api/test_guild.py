from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from core.models import Guild, Character


class GuildTestCase(APITestCase):
    def setUp(self):
        self.user_one = User.objects.create_user(username="userone", password="userone")
        self.user_two = User.objects.create_user(username="usertwo", password="usertwo")

    def test_list_guilds(self):
        Guild.objects.create(name="Test Guild", realm="Test Realm", faction="alliance")
        Guild.objects.create(name="Test Guild", realm="Test Realm", faction="alliance")
        Guild.objects.create(name="Test Guild", realm="Test Realm", faction="alliance")

        self.client.login(username="userone", password="userone")

        response = self.client.get(path=reverse("guilds-list")).json()

        self.assertEqual(len(response), 3)

    def test_create_guild(self):
        """Creating a guild should grant the user appropriate permissions."""
        self.client.login(username="userone", password="userone")

        response = self.client.post(
            path=reverse("guilds-list"),
            data={"name": "Test Guild", "realm": "Test Realm", "faction": "alliance"},
        )

        guild_response = response.json()
        guild = Guild.objects.get(pk=guild_response.get("id"))

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertTrue(self.user_one.has_perm("core.view_guild", guild))
        self.assertTrue(self.user_one.has_perm("core.view_guild_details", guild))
        self.assertTrue(self.user_one.has_perm("core.change_guild", guild))
        self.assertTrue(self.user_one.has_perm("core.delete_guild", guild))

        self.assertFalse(self.user_two.has_perm("core.view_guild", guild))
        self.assertFalse(self.user_two.has_perm("core.view_guild_details", guild))
        self.assertFalse(self.user_two.has_perm("core.change_guild", guild))
        self.assertFalse(self.user_two.has_perm("core.delete_guild", guild))

    def test_update_guild(self):
        self.client.login(username="userone", password="userone")

        data = {"name": "Test Guild", "realm": "Test Realm", "faction": "alliance"}

        response = self.client.post(path=reverse("guilds-list"), data=data).json()

        guild_id = response.get("id")

        updated = {"name": "Updated Name", "realm": "Updated Realm", "faction": "horde"}

        response = self.client.put(
            path=reverse("guilds-detail", kwargs={"pk": guild_id}), data=updated
        )

        updated_guild = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(updated_guild.get("name"), updated.get("name"))
        self.assertEqual(updated_guild.get("realm"), updated.get("realm"))
        self.assertEqual(updated_guild.get("faction"), updated.get("faction"))

        self.client.login(username="usertwo", password="usertwo")

        response = self.client.put(
            path=reverse("guilds-detail", kwargs={"pk": guild_id}), data=updated
        )

        self.assertEqual(response.status_code, 403)

    def test_delete_guild(self):
        self.client.login(username="userone", password="userone")

        data = {"name": "Test Guild", "realm": "Test Realm", "faction": "alliance"}

        response = self.client.post(path=reverse("guilds-list"), data=data).json()

        guild_id = response.get("id")

        response = self.client.delete(
            path=reverse("guilds-detail", kwargs={"pk": guild_id})
        )

        self.assertEqual(response.status_code, 204)

        response = self.client.post(path=reverse("guilds-list"), data=data).json()

        guild_id = response.get("id")

        self.client.login(username="usertwo", password="usertwo")

        response = self.client.delete(
            path=reverse("guilds-detail", kwargs={"pk": guild_id})
        )

        self.assertEqual(response.status_code, 403)


class GuildInviteTestCase(APITestCase):
    def setUp(self):
        self.user_one = User.objects.create_user(username="userone", password="userone")
        self.user_two = User.objects.create_user(username="usertwo", password="usertwo")
        self.user_three = User.objects.create_user(
            username="userthree", password="userthree"
        )

        self.client.login(username="userone", password="userone")
        self.client.post(
            path=reverse("guilds-list"),
            data={"name": "Test Guild", "realm": "Test Realm", "faction": "alliance"},
        )

        self.client.login(username="usertwo", password="usertwo")
        self.client.post(
            path=reverse("guilds-list"),
            data={
                "name": "Test Guild #2",
                "realm": "Test Realm #2",
                "faction": "alliance",
            },
        )
        self.client.post(
            path=reverse("characters-list"),
            data={
                "name": "Testgnome",
                "race": "gnome",
                "gender": "female",
                "class": "rogue",
                "role": "dd",
            },
        )

        self.client.login(username="userthree", password="userthree")
        self.client.post(
            path=reverse("characters-list"),
            data={
                "name": "Testtroll",
                "race": "troll",
                "gender": "female",
                "class": "rogue",
                "role": "dd",
            },
        )

    def test_list_invites(self):
        guild = Guild.objects.get(name="Test Guild")
        character = Character.objects.get(name="Testgnome")

        self.client.login(username="userone", password="userone")
        response = self.client.post(
            path=reverse("invites-list"),
            data={"guild": guild.pk, "invitee": character.pk},
        )

        self.assertEqual(response.status_code, 201)

        self.client.login(username="usertwo", password="usertwo")
        response = self.client.get(path=reverse("invites-list"))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)

        self.client.login(username="userthree", password="userthree")
        response = self.client.get(path=reverse("invites-list"))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 0)

    def test_create_invite(self):
        """TODO

        test creating invites for guild without permissions
        test creating invites for characters of the opposing faction
        """

    def test_accept_invite(self):
        """TODO"""
