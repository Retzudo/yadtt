from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

from api import views

router = DefaultRouter()
router.register("guilds", views.GuildViewSet, basename="guilds")
router.register("characters", views.CharacterViewSet, basename="characters")
router.register("invites", views.GuildInviteViewSet, basename="invites")
router.register("events", views.EventViewSet, basename="events")

urlpatterns = [path("auth/", obtain_jwt_token)] + router.urls
