from django.shortcuts import get_object_or_404
from guardian.shortcuts import assign_perm, get_objects_for_user
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import ParseError
from rest_framework.permissions import DjangoObjectPermissions
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from api import serializers, permissions, filters
from core import models
from core.models import GuildInvite, Character, Guild


class GuildViewSet(ModelViewSet):
    serializer_class = serializers.GuildSerializer
    permission_classes = (DjangoObjectPermissions,)

    def get_queryset(self):
        return get_objects_for_user(self.request.user, ["core.view_guild"])

    def perform_create(self, serializer):
        guild = serializer.save()
        assign_perm("core.view_guild", self.request.user, guild)
        assign_perm("core.view_guild_details", self.request.user, guild)
        assign_perm("core.change_guild", self.request.user, guild)
        assign_perm("core.delete_guild", self.request.user, guild)

    @action(
        detail=True,
        methods=["post"],
        url_path="removecharacter",
        permission_classes=(permissions.RemoveCharacterFromGuildPermission,),
    )
    def remove_character(self, request, pk=None):
        guild = self.get_object()
        character_id = request.data.get("character")
        character = get_object_or_404(Character, pk=character_id)

        guild.characters.remove(character)

        # To make sure the character pre_save signal is called
        character.guild = None
        character.save()

        return Response("", status=status.HTTP_204_NO_CONTENT)

    @action(
        detail=True,
        methods=["post"],
        url_path="awarddkp",
        permission_classes=(DjangoObjectPermissions,),
    )
    def award_dkp(self, request, pk=None):
        guild = self.get_object()
        dkp = request.data.get("dkp")
        description = request.data.get("description")
        character_ids = request.data.get("characters")
        characters = models.Character.objects.filter(pk__in=character_ids)

        for character in characters:
            models.Event.objects.create(
                guild=guild, character=character, dkp=dkp, description=description
            )

        return Response(
            {"message": f"Awarded {dkp} DKP to {len(characters)} characters"},
            status=status.HTTP_200_OK,
        )


class CharacterViewSet(ModelViewSet):
    serializer_class = serializers.CharacterSerializer
    queryset = Character.objects.all()
    permission_classes = (DjangoObjectPermissions,)
    filter_class = filters.CharacterFilter

    def perform_create(self, serializer):
        character = serializer.save(user=self.request.user)
        assign_perm("core.view_character", self.request.user, character)
        assign_perm("core.change_character", self.request.user, character)
        assign_perm("core.delete_character", self.request.user, character)


class GuildInviteViewSet(ModelViewSet):
    serializer_class = serializers.GuildInviteSerializer
    permission_classes = (permissions.GuildInvitePermission,)

    def get_queryset(self):
        # Return all invites a user can accept and invites that were created for a guild
        # the user has manage access to.
        can_accept = models.GuildInvite.objects.filter(invitee__user=self.request.user)
        manageable_guilds = get_objects_for_user(
            self.request.user, "core.change_guild", Guild, accept_global_perms=False
        )

        return can_accept.union(*[g.invites.all() for g in manageable_guilds])

    def perform_create(self, serializer):
        guild = serializer.validated_data.get("guild")
        invitee = serializer.validated_data.get("invitee")

        if guild.faction != invitee.faction:
            raise ParseError(
                "You cannot invite a character of the opposing faction to a guild."
            )

        serializer.save(inviter=self.request.user)

    @action(
        detail=True,
        methods=["post"],
        permission_classes=(permissions.AcceptGuildInvitePermission,),
    )
    def accept(self, request, pk=None):
        invite: GuildInvite = self.get_object()

        invite.guild.characters.add(invite.invitee)
        invite.delete()

        return Response("", status=status.HTTP_204_NO_CONTENT)


class EventViewSet(ModelViewSet):
    serializer_class = serializers.EventSerializer
    permission_classes = (permissions.EventPermission,)
    filter_fields = ("character",)

    def get_queryset(self):
        return models.Event.objects.filter(character__user=self.request.user)
