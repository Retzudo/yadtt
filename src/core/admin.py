from django.contrib import admin

from core import models


admin.site.register(models.Guild)
admin.site.register(models.Character)
admin.site.register(models.GuildInvite)
admin.site.register(models.Event)
