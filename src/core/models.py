import re
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator
from django.db import models

max_level = 60

classes = [
    ("druid", "Druid"),
    ("hunter", "Hunter"),
    ("mage", "Mage"),
    ("paladin", "Paladin"),
    ("priest", "Priest"),
    ("rogue", "Rogue"),
    ("shaman", "Shaman"),
    ("warlock", "Warlock"),
    ("warrior", "Warrior"),
]

factions = [("alliance", "Alliance"), ("horde", "Horde")]

roles = [("dd", "Damage Dealer"), ("healer", "Healer"), ("tank", "Tank")]

races = [
    (
        "Alliance",
        (
            ("dwarf", "Dwarf"),
            ("gnome", "Gnome"),
            ("human", "Human"),
            ("nightelf", "Night Elf"),
        ),
    ),
    (
        "Horde",
        (
            ("forsaken", "Forsaken"),
            ("orc", "Orc"),
            ("tauren", "Tauren"),
            ("troll", "Troll"),
        ),
    ),
]

genders = [("female", "Female"), ("male", "Male")]


def get_faction_races(faction_name):
    for faction, faction_races in races:
        if faction.lower() == faction_name.lower():
            return [race[0] for race in faction_races]

    return []


class Guild(models.Model):
    name = models.CharField(max_length=255)
    realm = models.CharField(max_length=255)
    faction = models.CharField(max_length=25, choices=factions)

    class Meta:
        permissions = (("view_guild_details", "View details"),)

    def __str__(self):
        return self.name

    @property
    def member_count(self):
        return self.characters.count()


class Character(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True, related_name="characters"
    )
    guild = models.ForeignKey(
        Guild,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="characters",
    )

    # 12 is WoW's name length limit
    name = models.CharField(
        max_length=12, validators=[RegexValidator("^[a-z]+$", flags=re.IGNORECASE)]
    )
    race = models.CharField(max_length=15, choices=races)
    gender = models.CharField(max_length=6, choices=genders)
    role = models.CharField(max_length=15, choices=roles)
    char_class = models.CharField(max_length=50, choices=classes)
    level = models.PositiveSmallIntegerField(
        default=max_level,
        validators=[MinValueValidator(1), MaxValueValidator(max_level)],
    )

    def __str__(self):
        if self.guild:
            return "{} <{}>".format(self.name, self.guild.name)
        else:
            return self.name

    @property
    def total_dkp(self):
        if not self.guild:
            return None

        return sum(event.dkp for event in self.events.filter(guild=self.guild))

    @property
    def faction(self):
        for faction, faction_races in races:
            for race, _ in faction_races:
                if self.race == race:
                    return faction.lower()


class GuildInvite(models.Model):
    guild = models.ForeignKey(Guild, on_delete=models.CASCADE, related_name="invites")
    inviter = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="invites_created"
    )
    invitee = models.ForeignKey(
        Character, on_delete=models.CASCADE, related_name="invites_received"
    )
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Guild Invite"

    def __str__(self):
        return "{}: {} invites {} ({} {})".format(
            self.guild.name,
            self.inviter.username,
            self.invitee.name,
            self.invitee.char_class,
            self.invitee.role,
        )


class Event(models.Model):
    character = models.ForeignKey(
        Character, on_delete=models.CASCADE, related_name="events"
    )
    guild = models.ForeignKey(Guild, on_delete=models.CASCADE, related_name="events")

    dkp = models.IntegerField()
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ("-created",)

    def __str__(self):
        if self.dkp > 0:
            return "+{} DKP: {}".format(self.dkp, self.description)
        if self.dkp < 0:
            return "{} DKP: {}".format(self.dkp, self.description)
        if self.dkp == 0:
            return self.description
