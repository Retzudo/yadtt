from django.contrib.auth.models import User, Group, Permission
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from guardian.shortcuts import assign_perm, remove_perm

from core.models import Character


@receiver(pre_save, sender=Character)
def on_character_pre_save(sender, instance: Character, **kwargs):
    # Force character name formatting
    instance.name = instance.name.lower().capitalize()

    # Manage guild permissions
    if instance.guild:
        assign_perm("core.view_guild_details", instance.user, instance.guild)

    if instance.pk:
        old_guild = Character.objects.get(pk=instance.pk).guild
        if old_guild:
            remove_perm("core.view_guild_details", instance.user, old_guild)


@receiver(post_save, sender=Character)
def on_character_post_save(sender, instance: Character, **kwargs):
    if instance.guild is None and instance.user is None:
        # Remove characters that have no guild and no user
        instance.delete()


@receiver(post_save, sender=User)
def on_user_post_save(sender, instance: User, created, **kwargs):
    if not created:
        return

    # Add user to the default user group
    instance.groups.add(get_default_user_group())


def get_default_user_group():
    group, _ = Group.objects.get_or_create(name="User")

    core_permissions = Permission.objects.filter(content_type__app_label="core")
    group.permissions.set(core_permissions)
    group.save()

    return group
