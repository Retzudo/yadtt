from django.contrib.auth.models import User, Group
from django.test import TestCase


class UserTestCase(TestCase):
    def setUp(self):
        User.objects.create_user(username="user", password="user")

    def test_default_group(self):
        group = Group.objects.get(name="User")

        self.assertIsNotNone(group)
        self.assertGreater(group.permissions.count(), 0)

    def test_user_has_default_group(self):
        """Users should be created with a default group set."""
        user = User.objects.get(username="user")

        group = user.groups.get(name="User")
        self.assertIsNotNone(group)
